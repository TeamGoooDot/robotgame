extends SpringArm

export var mouse_sensitivity = 0.05 

func _ready():
	set_as_toplevel(true)
	
func _unhandled_input(event: InputEvent) -> void: 
	if event is InputEventMouseMotion:
		follow_mouse(event)

func follow_mouse(event: InputEventMouseMotion) -> void: 
	rotation_degrees.x -= event.relative.y * mouse_sensitivity
	rotation_degrees.x = clamp(rotation_degrees.x, -90.0, 30.0)
	
	rotation_degrees.y -= event.relative.x * mouse_sensitivity
	rotation_degrees.y = wrapf(rotation_degrees.y, 0.0, 360.0)
