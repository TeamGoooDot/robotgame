extends KinematicBody

export var speed: float = 30.0 
export var jump_strength: float = 30.0
export var gravity: float = 50.0


var _velocity: Vector3 = Vector3.ZERO
var _snap_vector: Vector3 = Vector3.DOWN

onready var _spring_arm: SpringArm = $SpringArm
onready var _model: Spatial = $Model

func _physics_process(delta: float) -> void:
	movement(delta)
	

func movement(delta: float) -> void:
	var direction = Vector3.ZERO
	
	# Getting movement vectors for left/right and forwards/backwards
	direction.x = Input.get_action_strength("move_right") - Input.get_action_strength("move_left")
	direction.z = Input.get_action_strength("move_back") - Input.get_action_strength("move_forward")
	direction = direction.rotated(Vector3.UP, _spring_arm.rotation.y).normalized()
	# Applying current speed to movement vectors
	_velocity.x = direction.x * speed 
	_velocity.z = direction.z * speed 
	# Jumping
	if is_on_floor() and Input.is_action_just_pressed("jump"):
		_velocity.y  = jump_strength
		_snap_vector = Vector3.ZERO
	else:
		_velocity.y -= gravity * delta
		_snap_vector = Vector3.DOWN
	
	# Actually applying the movement
	_velocity = move_and_slide_with_snap(_velocity, _snap_vector, Vector3.UP, true)

	
	if _velocity.length() > 0.2: 
		var look_direction = Vector2(_velocity.z, _velocity.x)
		_model.rotation.y = look_direction.angle()
	
func _process(delta: float) -> void: 
	_spring_arm.translation = translation
